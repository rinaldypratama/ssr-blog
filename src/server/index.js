import express from 'express';
import renderer from './renderer';
import { matchRoutes } from 'react-router-config';
import Routes from '../shared/Routes';

import createStore from './createStore';
 

const app = express();

app.use(express.static('./build/public'));

app.get('*', (req, res) => {	
  const store = createStore;
	
	const promises = matchRoutes(Routes, req.path).map(({route}) => {
		 return route.loadData ? route.loadData(store) : null;
	});
	
	Promise.all(promises)
	.then(()=>{
		res.send(renderer(req, store));
	});

});

app.listen(3000, () => {
  console.log('Listening on port 3000');
});