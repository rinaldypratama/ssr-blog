import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import thunk from 'redux-thunk';

import postReducer from '../shared/store/postReducer';
import userReducer from '../shared/store/userReducer';

const rootReducer = combineReducers({
	user: userReducer,
	post: postReducer
});

const initialState = {};

const middleware = [thunk];
// eslint-disable-next-line
const composeMiddleware = compose(
	applyMiddleware(...middleware)
);

const store = createStore(
	rootReducer,
	initialState,
	composeMiddleware
);

export default store;