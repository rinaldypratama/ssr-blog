import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { renderRoutes } from 'react-router-config';
import { Provider } from 'react-redux';

import { setCurrentUser } from '../shared/actions/userActions';

import store from '../shared/store';

import Routes from '../shared/Routes';

if (localStorage.userLogin) {
  const payload = JSON.parse(localStorage.userLogin);
  // console.log("in", payload);
  store.dispatch(setCurrentUser(payload));
}

ReactDOM.hydrate(
	<Provider store={store}>
		<BrowserRouter>
			<div>{renderRoutes(Routes)}</div>
		</BrowserRouter>
	</Provider>
	, document.querySelector('#root')
);

