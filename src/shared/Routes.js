import React from 'react';
import './sass/index.scss';

import App from './App';

import Blog from './pages/Blog';
import About from './pages/About';
import Contact from './pages/Contact';
import AddPost from './pages/AddPost';
import DetailPost from './pages/DetailPost';
import LoginPage from './pages/LoginPage';
import page404 from './pages/page404';

export default [
	{
		...App,
		routes: [
		{
			...Blog,
			exact: true,
			path: '/',
		},
		{
			path: '/about',
			component: About
		},
		{
			path: '/contact',
			component: Contact
		},
		{
			path: '/add-post',
			component: AddPost
		},
		{
			...DetailPost,
			path: '/post/:title'
		},
		{
			path: '/login',
			component: LoginPage
		},
		{
			...page404
		}
		]
	}
];