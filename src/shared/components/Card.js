import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Card extends Component {
	render() {
		const { data } = this.props;
		const path = data.title.split(' ').join('-');
		// console.log("path",data);
		return (
			<div className="card-container clearfix">
				<img src={data.image} alt=""/>
				<div className="info">
					<p className="big-font bold-font wrap-title">{data.title}</p>
					<p className="bold-font">{data.author}</p>
					<p className="wrap-p">{data.contents}</p>
					<div className="more-btn bold-font"><Link to={`/post/${path}`}>Read More...</Link></div>
				</div>
			</div>
		);
	}
}

export default Card;