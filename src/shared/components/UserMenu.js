import React, { Component } from 'react';
import { Link } from 'react-router-dom';

const test='true';

class UserMenu extends Component {

	render() {
		const { auth, logout } = this.props;
		return (
			<ul>
				{
					auth ?
					<React.Fragment>								
					<li><Link to='/add-post'>Add Post</Link></li>
					<li onClick={logout} >Logout</li>
					</React.Fragment>:
					<li><Link to='/login'>Login</Link></li>
				}
			</ul>
		);
	}
}

export default UserMenu;