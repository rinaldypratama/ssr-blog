import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { withRouter } from 'react-router'

import { logoutUser } from '../actions/userActions'; 

import UserMenu from './UserMenu';

class Header extends Component {
	render() {
		const { user, logoutUser } = this.props;
		// const avaImage = user.isAuthenticated?
		// user.user.photoURL :
		// 'https://firebasestorage.googleapis.com/v0/b/ssr-blog-react.appspot.com/o/icon%2Favatar-159236_960_720.png?alt=media&token=63972d12-187f-4442-9584-f831fcb810e2';
		// console.log("ava", user.user.photoURL);
		return (
			<div className="header clearfix">
				<div className="wrapper">
					<div className="l-left">
						<h1>SSR<sub>BLOG</sub></h1>
					</div>
					<div className="l-right">
						<ul>
							<li><NavLink to="/" activeClassName="active-nav">Home</NavLink></li>
							<li><NavLink to="/about" activeClassName="active-nav">About</NavLink></li>
							<li><NavLink to="/contact" activeClassName="active-nav">Contact</NavLink></li>
						</ul>
						<div className="ava">
						<img  src='https://firebasestorage.googleapis.com/v0/b/ssr-blog-react.appspot.com/o/icon%2Favatar-159236_960_720.png?alt=media&token=63972d12-187f-4442-9584-f831fcb810e2' alt="avatar"/>
						<div className="menu-user">
							<UserMenu logout={logoutUser} auth={user.isAuthenticated}/>
						</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
  user:state.user
});



export default withRouter(connect(mapStateToProps, { logoutUser })(Header));