import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Footer extends Component {
	render() {
		return (
			<div className="footer clearfix">
				<div className="wrapper">
					<div className="l-left">
						<ul>
							<li><Link to='/'>Home</Link></li>
							<li><Link to='/about'>About</Link></li>
							<li><Link to='/contact'>Contact</Link></li>
						</ul>
					</div>
					<div className="l-right pad-right">
						<b>SSRBlog </b>@ 2018
					</div>
				</div>
			</div>
		);
	}
}

export default Footer;