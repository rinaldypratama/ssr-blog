import React, { Component } from 'react';
import { renderRoutes } from 'react-router-config';

import Header from './components/Header';
import Footer from './components/Footer';

const App =({route}) => {
  return (
    <div className="app">
      <Header/>
      <div className="wrapper container">
        {renderRoutes(route.routes)}
      </div>
      <Footer/>
    </div>
  )
};

export default {
  component:App
};

