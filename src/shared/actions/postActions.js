import axios from 'axios';

import { GET_POST, PICK_POST } from './types';

export const getPost = () => dispatch => {
  axios
    .get('http://localhost:3030/posts')
    .then(res => {
      dispatch({
        type: GET_POST,
        payload: res.data
      })
    })
    .catch(err =>
      console.log("error", err)
    );
};

export const pickPost = (param) => dispatch => {
  axios
    .get('http://localhost:3030/posts?title=',param)
    .then(res => {
      dispatch({
        type: PICK_POST,
        payload: res.data
      })
    })
    .catch(err =>
      console.log("error", err)
    );
};