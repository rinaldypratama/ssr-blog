import axios from 'axios';
import { firebaseAuth, googleProvider } from '../../../config/firebase';

import { SET_CURRENT_USER } from './types';

// Login - Get User Token
export const loginUser = () => dispatch => {
  firebaseAuth().signInWithPopup(googleProvider).then(result => {
      const token = result.credential.accessToken;
      const user = result.user;
      // console.log("auth", token, user, user.displayName);
      const payload = {
        token,
        user
      };
      localStorage.setItem('userLogin', JSON.stringify(payload));
        // console.log("payload",payload)
        dispatch(setCurrentUser(payload));
      // ...
    }).catch(error => {
      const errorCode = error.code;
      const errorMessage = error.message;
      const email = error.email;
      const credential = error.credential;
      alert(errorMessage);
    });
};


// Set logged in user
export const setCurrentUser = payload => {
  return {
    type: SET_CURRENT_USER,
    payload
  };
};

// Log user out
export const logoutUser = () => dispatch => {
  firebaseAuth().signOut().then(()=> {
    localStorage.removeItem('userLogin');
    dispatch(setCurrentUser({}));
  }).catch(error=> {
      alert('error');
  });
  
};
