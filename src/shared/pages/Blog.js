import React, { Component } from 'react';
import { connect } from 'react-redux';

import { getPost } from '../actions/postActions'; 

import Card from '../components/card';
import Load from '../utils/Loader';
import isEmpty from '../utils/isEmpty';

class Blog extends Component {
	componentDidMount(){
		window.scrollTo(0,0);
		this.props.getPost();
	};

	render() {
		const { posts } = this.props.post;
		// console.log("pos", posts);
		return (
			<div className="blog">
				<h1><i className="material-icons">date_range</i> Recent Post</h1>					
				{
					posts.map((x, index)=> <Card key="index" data={x}/>)
				}
			</div>
		);
	}
}

const mapStateToProps = state => ({
  post:state.post
});

function loadData(store){
	return store.dispatch(getPost());
};

export default {
	loadData,
	component: connect(mapStateToProps, { getPost })(Blog)
};