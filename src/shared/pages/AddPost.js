import React, { Component } from 'react';

class AddPost extends Component {
	constructor(props) {
    super(props);
    this.state = {
    	title:'',
    	image:'',
    	contents:''
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    window.scrollTo(0,0);
  }

  onChange(e){
  	this.setState({ [e.target.name]: e.target.value });
    // console.log(this.state);
  }

  onSubmit(e){
  	e.preventDefault();
  	console.log(initialValue);
  }

	render() {
		return (
			<div className="addpost">
				<h1>New Story</h1>
				<form >
					<label>Title</label>	
					<input 
						type="text" 
						placeholder="New Title" 
						name="title"
						onChange={this.onChange} 
						value={this.state.title}
					/>
					<br/>
					<label>Image</label>
					<input 
						type="url" 
						placeholder="https://www.image.com/100x100.jpg" 
						name="image"
						onChange={this.onChange} 
						value={this.state.image}
					/>
					<br/>
					<label>Contents</label>
					{/*<RichEditor value={this.onChangeEditor}/>*/}
					<textarea
						placeholder="Type here"
						onChange={this.onChange} 
						value={this.state.contents}
					/>
					<input 
						type="button" 
						value="Submit" 
						onClick={this.onSubmit} />
				</form>
			</div>
		);
	}
}

export  default AddPost;