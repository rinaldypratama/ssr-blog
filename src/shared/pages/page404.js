import React, { Component } from 'react';

class page404 extends Component {
	render() {
		return (
			<div className="page404">
				<h1>404 Page not Found</h1>
			</div>
		);
	}
}

export default {
	component:page404
};