import React, { Component } from 'react';
import { connect } from 'react-redux';

import { loginUser } from '../actions/userActions'; 

class LoginPage extends Component {
	constructor(props){
		super(props);

		this.login = this.login.bind(this);
	}
	componentDidMount() {
		if (this.props.user.isAuthenticated) {
			this.props.history.push('/');
		}
	}

	componentWillReceiveProps(nextProps) {
  	if (nextProps.user.isAuthenticated) {
    	this.props.history.push('/');
  	}
 	}

	login(){
		this.props.loginUser()
	}

	render() {
		return (
			<div className="login">
				<h1><i className="material-icons">info</i> Login</h1>
				<div className="google-btn" onClick={this.login}>
				<p>Sign in with Google</p>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
  user:state.user
});


export default connect(mapStateToProps, { loginUser })(LoginPage);