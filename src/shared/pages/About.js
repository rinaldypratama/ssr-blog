import React, { Component } from 'react';

export default class About extends Component {
	componentDidMount() {
    window.scrollTo(0,0);
  }
  
	render() {
		return (
			<div className="about">
				<h1><i className="material-icons">info</i> About</h1>

				<img className="l-right about-img" src="https://images.unsplash.com/photo-1525422847952-7f91db09a364?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=ce6622924dae3b9be067e1778a6b8707&auto=format&fit=crop&w=765&q=80" alt=""/>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit quidem fuga nisi nostrum! Ea neque eius saepe quaerat minus natus, nemo voluptas nesciunt labore dicta hic optio eveniet accusamus suscipit aut soluta, maxime quis esse nostrum repellendus, dolore assumenda molestiae.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni iusto vitae repellendus recusandae nostrum, cupiditate incidunt nam? Voluptas, voluptatem! Ad maiores inventore delectus distinctio officia numquam, quibusdam, recusandae blanditiis expedita error sunt quidem veniam. Ullam, architecto quod nobis ipsum aspernatur commodi quam odio reiciendis iusto blanditiis, dolores dolorem cupiditate ducimus sed. Voluptatem earum reiciendis temporibus suscipit accusamus delectus, magnam ea.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis ducimus ipsam enim adipisci maxime, facere quod quibusdam, quam. Ullam, quam!</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis blanditiis quos molestias modi et hic, nisi officia mollitia magni. Dolorem, dignissimos nulla, nesciunt blanditiis laborum earum sit repellendus delectus asperiores quaerat qui soluta numquam nobis culpa nihil, distinctio ipsum illo cumque beatae voluptas neque quae. Natus maiores provident cupiditate adipisci. Molestias doloribus tenetur distinctio nostrum amet! Odio ex rem recusandae ducimus sunt officia dolores dolor deleniti vel iure doloremque vero molestias iste, possimus assumenda rerum totam ea obcaecati nobis alias delectus cum tenetur. Facilis at distinctio quod harum, cumque aut nemo obcaecati in saepe magnam numquam possimus iste nisi autem.</p>
				
			</div>
		);
	}
}
