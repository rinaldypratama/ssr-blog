import React, { Component } from 'react';
import { connect } from 'react-redux';

import { pickPost } from '../actions/postActions';

class DetailPost extends Component {

	compomentDidMount(){
		window.scrollTo(0,0);
	}
	
	render() {
		return (
			<div className="detail-post">
				<h1>Titelnya Postingan ini</h1>
				<p className="pad-left">by <strong>Namanya ini</strong></p>
				<img src="https://i.imgur.com/eTuCPxM.jpg" alt=""/>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia tempora, vitae perspiciatis neque tempore quisquam facilis delectus molestiae doloribus consequatur, voluptatem? Quo cum aperiam eius ratione id repellendus sit eaque, earum laborum delectus ipsam cumque dolor aspernatur ea debitis, beatae laudantium quibusdam placeat dolores magni neque. Fugiat tempore facilis, aut. Ullam officia nulla minus aliquam rerum tenetur consequuntur at placeat est similique soluta consectetur aperiam fugiat, eligendi nostrum, dignissimos dolores! Quasi quibusdam cum molestias tenetur, atque quos, soluta alias odio, animi dolorem voluptatibus modi minus facilis rerum excepturi harum, suscipit nemo quisquam sed. Quaerat repudiandae voluptas facilis officia eligendi nihil obcaecati assumenda aspernatur accusantium at animi necessitatibus, veritatis aperiam reiciendis temporibus cum, voluptates, alias beatae voluptatem, nulla. Tempore nihil, suscipit expedita consequuntur id ea error facilis dolorum, molestias quod cum molestiae vitae veniam autem alias quos 
				<br/><br/>adipisci architecto ex numquam dolore, totam asperiores. Itaque consectetur nihil, minus voluptatibus laudantium unde minima recusandae molestias id perspiciatis esse pariatur voluptates molestiae dolores ex, quae voluptatum, deleniti reiciendis vel repellendus fugit tempore autem facere. Officiis adipisci deleniti, quod ipsa quae inventore non 
				<br/><br/>reiciendis quis reprehenderit, perspiciatis rerum accusantium, ea similique consequuntur soluta molestiae quasi eveniet corrupti expedita explicabo consequatur alias architecto est! Ullam, hic odit facere, adipisci expedita soluta enim. Ipsum voluptate similique ducimus libero, autem sit deleniti enim ut minus iste inventore neque accusantium nihil quos, eveniet hic! Laborum omnis, ut delectus qui recusandae itaque asperiores doloribus tenetur, quia eligendi similique impedit repellendus consequuntur, dolore harum sapiente. Facilis tempore nobis, nam maiores earum ipsam distinctio ipsa consequatur dolore dicta, explicabo doloribus, sequi eius at fugit natus iste quasi deserunt sed ullam laudantium. Hic placeat, consectetur molestias molestiae voluptatum quis accusantium porro dolores dicta dignissimos provident quae rerum vitae nam soluta voluptate fugit veniam corporis, inventore eius qui ducimus. Aspernatur reprehenderit 
				<br/><br/>eum voluptas perferendis, deleniti recusandae sit doloribus consectetur aliquid voluptatum praesentium reiciendis atque odio laudantium incidunt consequuntur porro dolorum illum, rem laborum quos. Porro iusto iure nulla, magnam esse voluptas impedit totam temporibus, asperiores, suscipit obcaecati tempora ut ullam distinctio. Inventore veniam laboriosam ratione assumenda corrupti quaerat consequatur eaque quae, alias at officiis corporis facere amet dicta ea rerum. Soluta quidem, labore. Mollitia alias molestiae animi laborum, magnam debitis, velit sint consequuntur itaque, assumenda, asperiores. Minima fugit repellat facere accusantium incidunt
				<br/><br/> modi ratione possimus praesentium fugiat impedit veritatis, debitis et dolorum architecto aliquam aliquid at deleniti, eligendi suscipit rerum provident, illo quisquam.
				</p>
			</div>
		);
	}
}

const mapStateToProps = state => ({
  post:state.post
});

// function loadData(store){
// 	const path = this.props.location.pathname.split('/');
// 	console.log("path", path[2].split("-").join(" "));
// 	return store.dispatch(pickPost(path[2]));
// };

export default {
	// loadData,
	component:connect(mapStateToProps, { pickPost })(DetailPost)
};