import React, { Component } from 'react';

export default class Contact extends Component {
	componentDidMount() {
    window.scrollTo(0,0);
  }
  
	render() {
		return (
			<div className="contact">
				<h1><i className="material-icons">contacts</i> Get In Touch</h1>
				<br/>
				<img src="https://images.unsplash.com/photo-1518043647833-3c0c5da3360f?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=e4ca6f099122b6c0eba295fa002b24f5&auto=format&fit=crop&w=613&q=80" alt=""/>
				<i className="material-icons">alternate_email</i>
				<p>rinaldyrizkypratama@gmail.com</p>
				<br/>
				<i className="material-icons">phone_iphone</i>
				<p>+6281-233-925-648</p>
				<br/>
				<i className="material-icons">location_on</i>
				<p>1885 Brick House Road Greensboro, GA 30642 USA</p>
				<br/>
			</div>
		);
	}
}
