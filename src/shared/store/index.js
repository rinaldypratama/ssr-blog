import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import thunk from 'redux-thunk';

import postReducer from './postReducer';
import userReducer from './userReducer';

const rootReducer = combineReducers({
	user: userReducer,
	post: postReducer
});

const initialState = window.INITIAL_STATE;

const middleware = [thunk];
// eslint-disable-next-line
const composeMiddleware = compose(
	applyMiddleware(...middleware),
	window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

const store = createStore(
	rootReducer,
	initialState,
	// applyMiddleware(...middleware)
	composeMiddleware
);

export default store;