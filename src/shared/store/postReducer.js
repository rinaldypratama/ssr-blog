import {  GET_POST, PICK_POST } from '../actions/types';

const initialState = {
	post:{},
	posts: []
};

export default (state = initialState, action) => {
	switch(action.type){
		case GET_POST:
			return {
				...state,
        		posts: action.payload
			};
		case PICK_POST:
	      return {
	        ...state,
	        post: action.payload
      };
		default:
			return state;
	}
}