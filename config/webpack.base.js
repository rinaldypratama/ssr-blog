const ExtractTextPlugin = require('extract-text-webpack-plugin');

const extractSass = new ExtractTextPlugin({
    filename: "[name].css",
    disable: process.env.NODE_ENV === "development"
});

module.exports = {
  module: {
    rules: [
      {
        test: /\.js?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        options: {
          presets: [
            'react',
            'es2015',
            'stage-0',
            ['env', { targets: { browsers: ['last 2 versions']}}]
            ]
        }
      },
      {
        test: /\.(scss)$/,
        use: extractSass.extract({
            use: [
                { loader: "css-loader"}, 
                { loader: "sass-loader"}
            ],
            fallback: "style-loader"
        })
      }
    ]
  },
  plugins: [
      extractSass
  ]

};