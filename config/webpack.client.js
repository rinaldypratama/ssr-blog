const path = require('path');
const merge = require('webpack-merge');
const baseConfig = require('./webpack.base.js');
// const BrowserSyncPlugin = require('browser-sync-webpack-plugin');

const config = {
  entry: './src/browser/index.js',

  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, '../build/public')
  }
};

module.exports = merge(baseConfig, config);