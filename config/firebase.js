import firebase from 'firebase';

const config = {
    apiKey: "AIzaSyCvbeDF2C9qjdiOmE5e2Qcaf1JwuYM6rWQ",
    authDomain: "ssr-blog-react.firebaseapp.com",
    databaseURL: "https://ssr-blog-react.firebaseio.com",
    projectId: "ssr-blog-react",
    storageBucket: "ssr-blog-react.appspot.com",
    messagingSenderId: "58511084908"
  };
  firebase.initializeApp(config);

export const googleProvider = new firebase.auth.GoogleAuthProvider();
export const githubProvider = new firebase.auth.GithubAuthProvider();
export const firebaseAuth = firebase.auth;